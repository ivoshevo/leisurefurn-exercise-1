﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.Data.SqlClient;
using System.Linq;

namespace Data
{
    class Program
    {
        //private static EFContext Context;
        //public Program(EFContext eF)
        //{
        //    Context = eF;
        //}
        static void Main(string[] args)
        {
            Save();
            Read();
        }
        
        static void Save()
        {

            //var Lines = File.ReadAllLines("data.csv");
            //foreach(var line in Lines)
            //{
            //    var values = line.Split(',');
            //    Data data = new Data()
            //    {
            //        InvoiceNumber = values[0],
            //        InvoiceDate = values[1],
            //        Address = values[2],
            //        InvoiceTotalExVAT = values[3],
            //        Linedescription = values[4],
            //        InvoiceQuantity = values[5],
            //        UnitsellingpriceexVAT = values[6]
            //    };

            //    Context.Datas.Add(data);
            //}


            var lineNumber = 0;
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localDB)\mssqllocaldb;Initial Catalog=DataDB;Integrated Security=True")) 
            {
                conn.Open();

                try
                {
                    using (StreamReader reader = new StreamReader(@"C:\Users\itzme\source\repos\Data\Data\bin\Debug\netcoreapp3.1\data.csv"))
                    {
                        
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            if (lineNumber != 0)
                            {
                                try
                                {
                                    var values = line.Split(',');
                                    var sql = "INSERT INTO DataDB.dbo.Datas VALUES ('" + values[0] + "','" + values[1] + "','" + values[2] + "','" + values[3] + "','" + values[4] + "','" + values[5] + "'," + values[6] + ")";
                                    var cmd = new SqlCommand();
                                    cmd.CommandText = sql;
                                    cmd.CommandType = System.Data.CommandType.Text;
                                    cmd.Connection = conn;
                                    cmd.ExecuteNonQuery();
                                }
                                catch(Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                    throw e;
                                }
                            }
                            lineNumber++;
                        }
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                conn.Close();
            }
            Console.WriteLine("Products Import Complete... Plase press ENTER KEY to Continue Reading files from database...!");
            
            Console.ReadLine();

            
        }
        static void Read()
        {
            EFContext eF = new EFContext();
            
            var valuse = from d in eF.Datas
                       select new { d.InvoiceNumber, d.InvoiceQuantity };
            foreach(var value in valuse)
            {
                Console.WriteLine($"Imported invoice number {value.InvoiceNumber} --- with total quantity {value.InvoiceQuantity}");
            }

            var TotalQuant = eF.Datas.Select(Q => Q.InvoiceQuantity).Sum();
            var TotalUnit = eF.Datas.Select(U => U.UnitsellingpriceexVAT).Sum();
            Console.WriteLine();
            Console.WriteLine($"Sum of InvoiceLines.Quantity * InvoiceLines.UnitSellingPriceExVAT: {TotalUnit * TotalQuant}" );
            Console.ReadLine();
        }
    }

    
    public class Data
    {
        [Key]
        public int Id { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDate { get; set; }
        public string Address { get; set; }
        public string InvoiceTotalExVAT { get; set; }
        public string Linedescription { get; set; }
        public int InvoiceQuantity { get; set; }
        public decimal UnitsellingpriceexVAT { get; set; }
    }

    public class EFContext : DbContext
    {
        private const string connectionString = "Server=(localdb)\\mssqllocaldb;Database=DataDB;Trusted_Connection=True;";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Data> Datas { get; set; }
    }

}
